<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('main');
});

Route::get('/admin', function()
{
	return View::make('admin');
});

Route::resource('user', 'UserController');
Route::post('adminlogin', array('as' => 'admin.login', 'uses' => 'LoginController@login'));
Route::post('checklogin', array('as' => 'admin.check', 'uses' => 'LoginController@check'));
Route::post('checkloginuser', array('as' => 'admin.checkuser', 'uses' => 'LoginController@checkuser'));
Route::resource('image', 'ImageController');
Route::post('image/gallery', array('as' => 'image.galleryUser', 'uses'=> 'ImageController@galleryUser'));
Route::post('image/publish', array('as' => 'image.publicarPendientes', 'uses' =>'ImageController@publicarPendientes'));
Route::post('image/updateuserid', array('as' => 'image.updateUserId', 'uses' => 'ImageController@updateUserId'));
Route::post('image/publicarFacebook', array('as' => 'image.publicarFacebook', 'uses' => 'ImageController@publicarFacebook'));
Route::post('image/obtenerModa', array('as' => 'image.obtenerModa', 'uses' => 'ImageController@obtenerModa'));


Route::group(array('before' => 'auth.token'), function()
{
	Route::resource('campaign', 'CampaignController');
	Route::resource('rfid', 'RfidController');
	Route::resource('send','SendController');
	Route::post('rfid/fileUpload', array('as' => 'rfid.fileupload', 'uses' => 'RfidController@rfidFileUpload'));
	Route::post('rfid/storeArriendo', array('as' => 'rfid.storeArriendo', 'uses' => 'RfidController@storeArriendo'));
	Route::resource('accessgallery','Accessgallery');

});