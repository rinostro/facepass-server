'use strict';

angular.module('facepass.perfil', [])

.config(['$routeProvider', function($routeProvider) {

	$routeProvider.when('/perfil', {
		templateUrl: 'app/perfil/perfil.html',
		controller: 'PerfilController'
	});
}])

.controller('PerfilController', function($scope, Session, ezfb, User, $cookieStore, $location, $window, $http) {


		updateLoginStatus();
		$scope.userData = User.get({id:$cookieStore.get("username")});
		$scope.userid= $cookieStore.get("username");
		
		$scope.imagesUnpublish=0;
		checkPublish($scope.userid);	

		$scope.postRegistro=false;
	

	function updateLoginStatus (more) {
		  ezfb.getLoginStatus(function (res) {
		  	if(res.status != "connected") {
		  		console.log("no conectado");
		  		$location.path('/preregistro');
		  	}
		    $scope.loginStatus = res;
		    (more || angular.noop)();
		  });
		}

	$scope.fblink = function(fblink) {
			$window.open(fblink);
	}


	function checkPublish(user_id){

		var statusPublish= new Image();
		statusPublish.user_id = user_id;
		statusPublish.status_publication= "0";

		$http.post('image/gallery', statusPublish).
			success(function(data, status, headers, config) {
					console.log("Mostar no publicadas aun: Success");
					console.log(data.images);
					if(data.images.length!=0){
						$scope.imagesUnpublish=data.images.length;
						console.log($scope.imagesUnpublish);
	
					}
			}).
			error(function(data, status, headers, config) {
					$scope.alerts.push({msg: data.message});
			})	

		}

	$scope.publicarPendientes= function(){

		var publicate= new Image();
		publicate.user_id = $scope.userid;
		publicate.status_publication= "0";			

		$http.post('image/publish', publicate).
			success(function(data, status, headers, config) {
					console.log("Publicadas las pendientes: Success");
					console.log(data.images);
			}).
			error(function(data, status, headers, config) {
					$scope.alerts.push({msg: data.message});
			})	



	}



}
);