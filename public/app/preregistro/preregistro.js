'use strict';

angular.module('facepass.preregistro', [])

.config(['$routeProvider', function($routeProvider) {

	$routeProvider.when('/preregistro', {
		templateUrl: 'app/preregistro/preregistro.html',
		controller: 'PreregistroController'
	});
}])

.controller('PreregistroController', function($scope, $location, ezfb, User, Session, $cookieStore) {
	$scope.cargando = false;
	var status = $cookieStore.get("access_token");
	
	if(status != null) {
		$location.path('/perfil');
	}

	$scope.login = function (rut) {
    /**
     * Calling FB.login with required permissions specified
     * https://developers.facebook.com/docs/reference/javascript/FB.login/v2.0
     */
     $scope.cargando = true;
	    ezfb.login(function (res) {
	      /**
	       * no manual $scope.$apply, I got that handled
	       */
			if (res.authResponse) {
				var user = new User();
                user.rut = rut;
                user.access_token = res.authResponse.accessToken;
                user.$save(function(data){
                    Session.create(data.user.access_token, data.user.id);
                    $scope.cargando = false;
                    $location.path('/perfil');
                }, function(data){
                });
				updateLoginStatus();
			}
	    }, {scope: ['email', 'public_profile', 'user_friends', 'publish_actions']});
  	};

	$scope.logout = function () {
	/**
	 * Calling FB.logout
	 * https://developers.facebook.com/docs/reference/javascript/FB.logout
	 */
		ezfb.logout(function () {
			updateLoginStatus();
		});
	};
  
  /**
   * Update loginStatus result
   */
	function updateLoginStatus (more) {
		ezfb.getLoginStatus(function (res) {
			$scope.loginStatus = res;
			(more(res) || angular.noop)();
		});
	}

})

.directive('validacionrut', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.integer = function(modelValue, viewValue) {
        if (ctrl.$isEmpty(modelValue)) {
          // consider empty models to be valid
          return true;
        }

        if (true) {
        	//aqui validacion rut
          	// it is valid
          return true;
        }

        // it is invalid
        return false;
      };
    }
  };
});