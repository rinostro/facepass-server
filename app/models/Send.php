<?php

class Send extends \Eloquent {
	protected $fillable = [];
    public $incrementing = false;

    public function rfid()
    {
        return $this->belongsTo('Rfid');

	}
    public function campaign()
    {
        return $this->belongsTo('Campaign');
    }  
}