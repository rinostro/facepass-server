<?php

class Campaign extends \Eloquent {
	protected $fillable = ['campaign_id'];
	public $incrementing = false;

	public function images()
    {
        return $this->hasMany('Images');
    }
}