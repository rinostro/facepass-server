'use strict';

angular.module('facepass.registromail', ['ezfb'])

.config(['$routeProvider', 'ezfbProvider', function($routeProvider, ezfbProvider) {

	$routeProvider.when('/registromail', {
		templateUrl: 'app/registromail/registromail.html',
		controller: 'RegistroMailController'

	});

	ezfbProvider.setLocale('es_LA');

	ezfbProvider.setInitParams({
	    // This is my FB app id for plunker demo app
	    appId: '1522630301321893',

	    // Module default is `v1.0`.
	    // If you want to use Facebook platform `v2.0`, you'll have to add the following parameter.
	    // https://developers.facebook.com/docs/javascript/reference/FB.init/v2.0
	    version: 'v2.0'
	  });  
}])

.controller('RegistroMailController', function($scope, ezfb, $window, $location, User,Session, $routeParams,Rfid) {

	$scope.alerts = [];
	$scope.rfid= $routeParams.rfid;
	console.log($scope.rfid);

	var setUser = function(userData) {
		SessionData.userData = userData;
	}

	updateLoginStatus(updateApiMe);

	$scope.login = function () {


		/**
		 * Calling FB.login with required permissions specified
		 * https://developers.facebook.com/docs/reference/javascript/FB.login/v2.0
		 */
		ezfb.login(function (res) {
		  /**
		   * no manual $scope.$apply, I got that handled
		   */
		  if (res.authResponse) {
		  	
				var user = new User();
				user.rut = $scope.rut;
				user.access_token = res.authResponse.accessToken;
				user.rfid = $scope.rfid;
				user.$save(function(data){
					 Session.create(data.user.access_token, data.user.id);
                	$location.path('/perfil');
					setUser(data.user);
				}, function(data){
				});
				//respuesta de save

		    updateLoginStatus(updateApiMe);
		  }
		}, {scope: ['email', 'public_profile', 'user_friends', 'publish_actions']});

	};

	$scope.logout = function () {
	/**
	 * Calling FB.logout
	 * https://developers.facebook.com/docs/reference/javascript/FB.logout
	 */
	ezfb.logout(function () {
	  updateLoginStatus(updateApiMe);
	});
	};

	$scope.share = function () {
	ezfb.ui(
	  {
	    method: 'feed',
	    name: 'angular-easyfb API demo',
	    picture: 'http://plnkr.co/img/plunker.png',
	    link: 'http://plnkr.co/edit/qclqht?p=preview',
	    description: 'angular-easyfb is an AngularJS module wrapping Facebook SDK.' + 
	                 ' Facebook integration in AngularJS made easy!' + 
	                 ' Please try it and feel free to give feedbacks.'
	  },
	  function (res) {
	    // res: FB.ui response
	  }
	);
	};

	/**
	* For generating better looking JSON results
	*/
	var autoToJSON = ['loginStatus', 'apiMe']; 
	angular.forEach(autoToJSON, function (varName) {
	$scope.$watch(varName, function (val) {
	  $scope[varName + 'JSON'] = JSON.stringify(val, null, 2);
	}, true);
	});

	/**
	* Update loginStatus result
	*/
	function updateLoginStatus (more) {
		ezfb.getLoginStatus(function (res) {
			$scope.loginStatus = res;			
		  	(more || angular.noop)();
		});
	}

	/**
	* Update api('/me') result
	*/
	function updateApiMe () {
		ezfb.api('/me', function (res) {
			console.log(res);
		  	$scope.apiMe = res;
		});
	}
})

.directive('validacionrut', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.integer = function(modelValue, viewValue) {
        if (ctrl.$isEmpty(modelValue)) {
          // consider empty models to be valid
          return true;
        }

        if (true) {
        	//aqui validacion rut
          	// it is valid
          return true;
        }

        // it is invalid
        return false;
      };
    }
  };
});