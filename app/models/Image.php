<?php

class Image extends \Eloquent {
	protected $fillable = ['campaign_id', 'totem_id', 'rfid', 'image_checksum'];

	public function user()
    {
        return $this->belongsTo('User');
    }

    public function campaign()
    {
        return $this->belongsTo('Campaign');
    }
}