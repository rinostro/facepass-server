'use strict';

angular.module('facepass.services', [])

.factory('User', function($resource, $http) {
    return $resource('/user/:id'); // Note the full endpoint address
})

.factory('Campaign', function($resource, $http) {
    return $resource('/campaign/:id');
})

.factory('Rfid', function($resource, $http) {
    return $resource('/rfid/:id');
})

.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}])

.service('AdminSession', ['$http', '$location', '$cookieStore', function ($http, $location, $cookieStore) {
    this.create = function (access_token, username) {
        this.showNavbar = true;
        this.loginPage = false;
        this.access_token = access_token;
        this.username = username;
        $cookieStore.put('admin_token', access_token);
        $cookieStore.put('admin_username', username);
        var args = {};
        //rootscope.$emit('loginEvent', args);
    };
    this.destroy = function () {
        this.showNavbar = false;
        this.loginPage = true;
        this.access_token = null;
        this.username = null;
        $cookieStore.put('admin_token', null);
        $cookieStore.put('admin_username', null);
    };
    return this;
}])

.service('Session', ['$http', '$location', '$cookieStore', function ($http, $location, $cookieStore) {
    this.create = function (access_token, username) {
        this.showNavbar = true;
        this.loginPage = false;
        this.access_token = access_token;
        this.username = username;
        $cookieStore.put('access_token', access_token);
        $cookieStore.put('username', username);
        var args = {};
        //rootscope.$emit('loginEvent', args);
    };
    this.destroy = function () {
        this.showNavbar = false;
        this.loginPage = true;
        this.access_token = null;
        this.username = null;
        $cookieStore.put('access_token', null);
        $cookieStore.put('username', null);
    };
    return this;
}]);