<?php

class LoginController extends \BaseController {

	public function login()
	{
		$user_name=Input::get('user_name');
		$password=Input::get('password');

		$user=User::where('username',$user_name)->where('password',$password)->where('type', 'admin')->first();
		if(empty($user)){
			return Response::json(
		    	array('error' => true, 'message' => 'Login incorrecto'),
		        400
		    );

		}
		else{
			return Response::json(
		    	array('error' => true, 'message' => 'Admin logeado','token'=>$user->access_token, 'username' => $user->username),
		        200
		    );
		}
	}

	public function check()
	{
		$token=Input::get('token');

		$user=User::where('access_token',$token)->first();
		if(empty($user) || $user->type != 'admin'){
			return Response::json(
		    	array('error' => true, 'message' => 'Login incorrecto'),
		        400
		    );
		} else {
			return Response::json(
		    	array('error' => false, 'message' => 'ok'),
		        200
		    );
		}
	}

	public function checkuser()
	{
		$token=Input::get('token');

		$user=User::where('access_token',$token)->first();
		if(empty($user) || $user->type != 'user'){
			return Response::json(
		    	array('error' => true, 'message' => 'Login incorrecto'),
		        400
		    );
		} else {
			return Response::json(
		    	array('error' => false, 'message' => 'ok'),
		        200
		    );
		}
	}

}