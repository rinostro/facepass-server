'use strict';

angular.module('facepass.campaign', ['ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {

	$routeProvider.when('/campaigns', {
		templateUrl: 'app/campaigns/campaign.html',
		controller: 'CampaignController'
	});
}])

.controller('CampaignController', function($scope, $modal, Campaign,$resource ) {
	
  $scope.maxSize = 5;
  $scope.bigTotalItems = 175;
  $scope.bigCurrentPage = 1;
  $scope.numPages = 3;

  var campaigns = Campaign.query(function() {
      $scope.campaigns = campaigns;
      //$scope.totalItems = 64;
      //$scope.currentPage = 1;
      $scope.maxSize = 5;
      $scope.bigTotalItems = campaigns.length;
      $scope.bigCurrentPage = 1;
  });

  $scope.edit={};
  

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function() {
    console.log('Page changed to: ' + $scope.currentPage);
  };

  $scope.open = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'addcampaign.html',
      controller: 'ModalAddCampaign',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      //$scope.selected = selectedItem;
      //$scope.campaigns.push($scope.selected.name);
      var campaigns = Campaign.query(function() {
      $scope.campaigns = campaigns;
      });


    }, function () {
      console.info('Modal dismissed at: ' + new Date());
    });
  };


  $scope.editarCampaign = function (size,id_campaign) {

    var modalInstance = $modal.open({
      templateUrl: 'editcampaign.html',
      controller: 'ModalEditCampaign',
      size: size,
      resolve: {
        id: function () {
          return id_campaign;
        }
      }
    });

    modalInstance.result.then(function (campaign_edit) {

      var campaigns = Campaign.query(function() {
      $scope.campaigns = campaigns;
      });


    }, function () {
      console.info('Modal dismissed at: ' + new Date());
    });
  };


})


.controller('ModalAddCampaign', function ($scope, $modalInstance, Campaign, $http) {

  $scope.guardar = function(data) {
    $http.post('/campaign', data).
    success(function(data, status, headers, config) {
      $modalInstance.close(data);
    }).
    error(function(data, status, headers, config) {
      $scope.alerts.push({msg: data.message});
    })
  };

  $scope.cancel = function() {
    $scope.campaign = {};
    $modalInstance.dismiss('cancel');
  };

  $scope.ok = function () {
    //$modalInstance.close();
  };

})

.controller('ModalEditCampaign', function ($scope, $resource, $modalInstance, Campaign, $http, id) {
         $scope.edit={};

      Campaign.get({id: id}, function(data){      //Invoco a show de REST de Laravel
      
        if(data.error==false){
          $scope.edit.name=data.campaign.name;
          $scope.edit.startdate=new Date(data.campaign.start_date);
          $scope.edit.enddate=new Date(data.campaign.end_date);
        }
         console.log(data.message);
         
   });  //Fin de User.get({id: id}, function(data){


  $scope.guardar = function(data) {

      var Campaign_edit = $resource('/campaign/:id/edit?name='+$scope.edit.name+
                                                        '&start_date='+$scope.edit.startdate+
                                                        '&end_date='+$scope.edit.enddate
                                                        ,{id:'@id'});
      Campaign_edit.get({id:id}, function(data){
      
      console.log(data.message);
      if(data.error!=true)
        $modalInstance.close({error:data.error, campaign:$scope.edit});   //close(result), en el cierre envio una respuesta al padre
      });
   
  };

  $scope.cancel = function() {
    $scope.campaign = {};
    $modalInstance.dismiss('cancel');
  };

  $scope.ok = function () {
    //$modalInstance.close();
  };

});




