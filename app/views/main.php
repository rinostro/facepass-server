<!DOCTYPE html>
<html lang="es" ng-app="facepass">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Pass</title>

    <link rel="stylesheet" href="css/style.css">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
     <script src="bower_components/alertify.js/lib/alertify.min.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <!--
    <link href="css/main.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link href="css/cover.css" rel="stylesheet">
    -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <div class="full-width" ng-controller="HeaderController">
    
        <header>
            <div class="container">
                <nav class="main-nav">
                    <!-- <ul ng-hide="!loginStatus || loginStatus.status != 'connected'"> -->
                    <ul>
                        <li class="active"><a href="" ng-show="status != null" ng-click="logout()"><i class="fa fa-power-off"></i> Log Out</a></li>
                    </ul>
               </nav> <!-- .main-nav -->
           </div> <!-- .container -->
        </header>

        <div class="container">
            <div ng-view></div>
        </div> <!-- .container -->

    </div> <!-- .full-width -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/angular-route/angular-route.min.js"></script>
    <script src="bower_components/angular-resource/angular-resource.min.js"></script>
    <script src="bower_components/angular-cookies/angular-cookies.min.js"></script>
    <script src="bower_components/angular-easyfb/angular-easyfb.min.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
   
    <script src="app/app.js"></script>
    <script src="app/services/services.js"></script>
    <script src="app/directives.js"></script>
    <script src="app/preregistro/preregistro.js"></script>
    <script src="app/perfil/perfil.js"></script>
    <script src="app/registromail/registromail.js"></script>
    <script src="app/postregistro/postregistro.js"></script>
</body>
</html>
