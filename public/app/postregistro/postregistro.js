'use strict';

angular.module('facepass.postregistro', ['ezfb','ui.bootstrap'])


.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
})




.config(['$routeProvider', 'ezfbProvider', function($routeProvider, ezfbProvider) {

	$routeProvider.when('/postregistro', {
		templateUrl: 'app/postregistro/postregistro.html',
		controller: 'PostregistroController'

	});

	ezfbProvider.setLocale('es_LA');

	ezfbProvider.setInitParams({
	    // This is my FB app id for plunker demo app
	    appId: '1522630301321893',

	    // Module default is `v1.0`.
	    // If you want to use Facebook platform `v2.0`, you'll have to add the following parameter.
	    // https://developers.facebook.com/docs/javascript/reference/FB.init/v2.0
	    version: 'v2.0'
	  });  
}])




.controller('PostregistroController', function($scope, ezfb, $window, $location, User,$routeParams,Rfid, Session, $http,$resource, $cookieStore) {

	$scope.alerts = [];
	$scope.campaignOk;
	$scope.gallery = false;
	$scope.campaign= $routeParams.campaign;
	
	$scope.images;
	//$scope.rfids_image=[];

	$scope.page=1;

	$scope.escogidas=[];
	$scope.rfid_escogidas=[];
	$scope.images_name=[];

	$scope.token="";
	$scope.userid="";

	$scope.cargando = false;
	$scope.userData = User.get({id:$cookieStore.get("username")});

		
	//Pagination
	
	$scope.currentPage = 0;
    $scope.pageSize = 10;

    $scope.imagesPage=[];


    $scope.imageName=function(image){

    	var index = $scope.images.indexOf(image);
 		
 		return $scope.images_name[index];

    }
    $scope.numberOfPages=function(){

    	$scope.imagesPage=[];

    	if ($scope.pageSize>50)
    		$scope.pageSize=50;
    	
    	for(var i=$scope.currentPage; i<$scope.pageSize;i++)
    		$scope.imagesPage.push($scope.images[i]);

        return Math.ceil($scope.images.length/$scope.pageSize);                
    }

//Determino si existe la campania de antemano, si es asi, puedo mostrar la galeria asociada
	$http.get('/campaign').
    success(function(data, status, headers, config) {
    	//Si existe la campaña, actualizo variable
    	if(data.indexOf($scope.campaign)!=-1){
    	$scope.campaignOk = true;
    	}
    	else{
    	$scope.campaignOk=false;
    	//$location.path('/preregistro');
    	}
    }).
    error(function(data, status, headers, config) {
        console.log("Error");
          //$location.path('/');
    })
    
//Obtengo imagenes asociadas a una campaña que no tengan un id de user
	var Images= $resource('/image:id', {campaign_id:'@id'});
	var result=Images.get({campaign_id:$scope.campaign},function(){

		console.log("Imagenes obtenidas");
		$scope.images=result.images;

		$scope.images_name= result.images_name;

		//console.log(result.checksum);
		//console.log(result.rfids_image);
		//$scope.rfids_image= result.rfids_image;

		console.log($scope.images);
	});
	

	var setUser = function(userData) {
		SessionData.userData = userData;
	}
	//updateLoginStatus(updateApiMe);


//Al presionar una imagen, guardo las ids escogidas
 	$scope.isChoosen=function(choosen){

 		var index = $scope.escogidas.indexOf(choosen);
 		var id_image="";


 		if( index == -1){

 			$scope.escogidas.push(choosen);
 			//$scope.rfid_escogidas.push($scope.rfids_image[$scope.images.indexOf(choosen)]);

 			console.log($scope.escogidas);
 			//console.log($scope.rfid_escogidas);

 			//Le agrego una clase para mostrar borde
 			id_image= "#"+ choosen.toString();
 			$(id_image).removeClass('border-image');
 			$(id_image).toggleClass('choosen-image');
 		
 		}
 		else{

 			$scope.escogidas.splice(index, 1);
 			//$scope.rfid_escogidas.splice(index,1);
 			console.log($scope.escogidas);
 			//console.log($scope.rfid_escogidas);

 			//Le quito la clase
 			id_image= "#"+ choosen.toString();
 			$(id_image).removeClass('choosen-image');
 			$(id_image).toggleClass('border-image');

 		}
 	};

 

  //Cuando desee publicar
 	$scope.publicar=function(){

 		$scope.cargando = true;
		if(($scope.escogidas).length==0){
 				console.log("No hay imagenes escogidas");
 				alertify.alert("No hay imagenes escogidas");
 		}
 		else{

 			var moda=-1;
 			var obtenermoda= new Image();
					
			obtenermoda.ids_images=$scope.escogidas;
			$http.post('image/obtenerModa', obtenermoda).
							success(function(data, status, headers, config) {
								console.log("Success en obtener moda");
								$scope.rfid_escogidas= data.rfids_image;

								//Genero un array unico
 			var rfid_todos=[];
 			var lista_temp=[];
 			for(var i=0;i<($scope.escogidas).length;i++){
 					lista_temp=$scope.rfid_escogidas[i];

 				for(var j=0; j<lista_temp.length;j++){
					rfid_todos.push(lista_temp[j]);
 				}

 			}
 			console.log(rfid_todos);



 			//Encuentro la moda
 			var frecuenciaTemp;
 			var frecuenciaModa=0;
 			

 			for(var i=0; i<rfid_todos.length; i++){
 				frecuenciaTemp=1;
 				for(var j=1; j<rfid_todos.length;j++){
 					if(rfid_todos[i]==rfid_todos[j])
 						frecuenciaTemp++;
 				}
 				if(frecuenciaTemp>frecuenciaModa){
 					frecuenciaModa=frecuenciaTemp;
 					moda=rfid_todos[i];
 				}

 			}//fin for

 			rfid_todos=null;
 			lista_temp=null;
			

 			console.log("La moda es "+moda);

							}).
							error(function(data, status, headers, config) {
								$scope.alerts.push({msg: data.message});
							})
 			
 			
 		


 			//Actualizo rfid con el usuario
	 		if(moda!="0000000000"){
	 			var rfid= new Rfid();
	 			rfid.id=moda;
	 			rfid.userid=$cookieStore.get("username");; //le envio id de usuario a traves del rut
	 			//rfid.userid=$scope.userid;
				rfid.$save(function(data){
					$scope.alerts.push({msg: data.message});
					$scope.rfid = {};
				},
				function(data){
					$scope.alerts.push({msg: data.data.message});
				});


				//Actualizar imagenes
				var imgupdate= new Image();
				imgupdate.rfid = moda;
				imgupdate.user_id= $cookieStore.get("username");   //Es el id del usuario
				//imgupdate.user_id=$scope.userid;
				imgupdate.campaign_id= $scope.campaign;

				$http.post('image/updateuserid', imgupdate).
					success(function(data, status, headers, config) {
						console.log("Success");
					}).
					error(function(data, status, headers, config) {
						$scope.alerts.push({msg: data.message});
					})

					//Postear en Facebook las fotos
					for(var i=0; i<($scope.escogidas).length;i++){
						var updateunico= new Image();
						//updateunico.user_id=$scope.userid;
						updateunico.user_id=$cookieStore.get("username");
						updateunico.image_id=$scope.escogidas[i];
					

						$http.post('image/publicarFacebook', updateunico).
							success(function(data, status, headers, config) {
								console.log("Success");
							}).
							error(function(data, status, headers, config) {
								$scope.alerts.push({msg: data.message});
							})
					}

			        //$window.open($scope.userData.link);
			          $scope.rfids_image=null;
                      $scope.rfid_escogidas=null;
                      $scope.images=null;
                      $scope.images_name=null;
                      $scope.imagesPage=null;
                      alertify.set({ delay: 15000 });
                      alertify.log("Publicando, espere...por favor");
                      $location.path('/perfil');

				}

				else{

					//Postear en Facebook en el caso especial sin asignarlas a nadie las fotos
					for(var i=0; i<($scope.escogidas).length;i++){
						var updateunico= new Image();
						//updateunico.user_id=$scope.userid;
						updateunico.user_id=$cookieStore.get("username");
						updateunico.image_id=$scope.escogidas[i];
					

						$http.post('image/publicarFacebook', updateunico).
							success(function(data, status, headers, config) {
								console.log("Success");
							}).
							error(function(data, status, headers, config) {
								$scope.alerts.push({msg: data.message});
							})
					}//fin for
					
					
                    //$window.open($scope.userData.link);
                      $scope.rfids_image=null;
                      $scope.rfid_escogidas=null;
                      $scope.images=null;
                      $scope.images_name=null;
                      $scope.imagesPage=null;
                      alertify.set({ delay: 15000 });
                      alertify.log("Publicando, espere...por favor");
                      $location.path('/perfil');
				

				}//fin else

			
 		}//fin else
 	}//fin function


	$scope.login = function () {

	    $scope.cargando = true;
		/**
		 * Calling FB.login with required permissions specified
		 * https://developers.facebook.com/docs/reference/javascript/FB.login/v2.0
		 */
		ezfb.login(function (res) {
		  /**
		   * no manual $scope.$apply, I got that handled
		   */
		  if (res.authResponse) {
		  	
				var user = new User();
				user.rut = $scope.rut;
				user.access_token = res.authResponse.accessToken;
				//$scope.token=user.access_token;
				//$scope.userid=user.rut;
				
				user.$save(function(data){
				Session.create(data.user.access_token, data.user.id);
                   
					$scope.gallery = true;
					console.log($scope.gallery);
					$scope.campaignOk = true;
					$scope.cargando = false;
    				console.log($scope.campaignOk);
				}, function(data){
					console.log(data);
					
					$location.path('/postregistro');
				});
				//respuesta de save

		    updateLoginStatus(updateApiMe);
		  }
		}, {scope: ['email', 'public_profile', 'user_friends', 'publish_actions']});

	};

	
	$scope.logout = function () {
	/**
	 * Calling FB.logout
	 * https://developers.facebook.com/docs/reference/javascript/FB.logout
	 */
	ezfb.logout(function () {
	  updateLoginStatus(updateApiMe);
	});
	};

	$scope.share = function () {
	ezfb.ui(
	  {
	    method: 'feed',
	    name: 'angular-easyfb API demo',
	    picture: 'http://plnkr.co/img/plunker.png',
	    link: 'http://plnkr.co/edit/qclqht?p=preview',
	    description: 'angular-easyfb is an AngularJS module wrapping Facebook SDK.' + 
	                 ' Facebook integration in AngularJS made easy!' + 
	                 ' Please try it and feel free to give feedbacks.'
	  },
	  function (res) {
	    // res: FB.ui response
	  }
	);
	};


	/**
	* For generating better looking JSON results
	*/
	var autoToJSON = ['loginStatus', 'apiMe']; 
	angular.forEach(autoToJSON, function (varName) {
	$scope.$watch(varName, function (val) {
	  $scope[varName + 'JSON'] = JSON.stringify(val, null, 2);
	}, true);
	});

	/**
	* Update loginStatus result
	*/
	function updateLoginStatus (more) {
		ezfb.getLoginStatus(function (res) {
			$scope.loginStatus = res;	
			(more || angular.noop)();
		});
	}

	/**
	* Update api('/me') result
	*/
	function updateApiMe () {
		ezfb.api('/me', function (res) {
			console.log(res);
		  	$scope.apiMe = res;
		  //	$scope.userid=res.id;
		});
	}
})

.directive('validacionrut', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.integer = function(modelValue, viewValue) {
        if (ctrl.$isEmpty(modelValue)) {
          // consider empty models to be valid
          return true;
        }

        if (true) {
        	//aqui validacion rut
          	// it is valid
          return true;
        }

        // it is invalid
        return false;
      };
    }
  };
});

