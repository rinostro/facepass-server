<!DOCTYPE html>
<html lang="es" ng-app="facepass.admin">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Pass</title>

    <link rel="stylesheet" href="css/style.css">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
     <script src="bower_components/alertify.js/lib/alertify.min.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Custom styles for this template -->
    <!--
    <link href="css/main.css" rel="stylesheet">
    <link href="css/estilo.css" rel="stylesheet">
    <link href="css/cover.css" rel="stylesheet">
    -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="admin-page">

    <div class="full-width">
    
   <!--      <header class="admin-header">
            <div class="container" ng-controller="HeaderController">
                <nav class="main-nav">
                    <ul class="nav masthead-nav">
                        <li class="active"><a href="" ng-click="logout()">Log Out</a></li>
                    </ul>
               </nav> 
            </div> 
        </header> -->

  <header class="admin-header">
         <!--Menu navegacion superior -->
          <nav class="navbar navbar-default" role="navigation">
        <!-- El logotipo y el icono que despliega el menú se agrupan
             para mostrarlos mejor en los dispositivos móviles -->
        <div class="navbar-header">
          <a class="navbar-brand">FotoLink Admin</a>
        </div>
       
          <ul class="nav navbar-nav">
            <li class="active"><a href="#/rfids">RFID</a></li>
            <li><a href="#/campaigns">Campañas</a></li>
              <li class="active"><a href="#/registro">Registro</a></li>
          </ul>
         </nav>

      <!--Boton logout -->
           <div class="container" ng-controller="HeaderController">
                <nav class="main-nav">
                    <ul class="nav masthead-nav">
                        <li class="active"><a href="" ng-click="logout()">Log Out</a></li>
                    </ul>
               </nav> 
            </div> 
    </header>

    <!--Contenedor principal -->
        <div class="container custom">
            <div ng-view></div>
        </div> <!-- .container -->

    </div> <!-- .full-width -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/angular-route/angular-route.min.js"></script>
    <script src="bower_components/angular-resource/angular-resource.min.js"></script>
    <script src="bower_components/angular-cookies/angular-cookies.min.js"></script>
    <script src="bower_components/angular-easyfb/angular-easyfb.min.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="app/appadmin.js"></script>
    <script src="app/services/services.js"></script>
    <script src="app/directives.js"></script>
    <script src="app/adminlogin/adminlogin.js"></script>
    <script src="app/registro/registro.js"></script>
    <script src="app/campaigns/campaign.js"></script>
    <script src="app/rfids/rfids.js"></script>
</body>
</html>
