'use strict';

angular.module('facepass.admin', [
	'ngRoute',
	'ngResource',
	'ngCookies',
	'facepass.services',
	'facepass.directives',
  'facepass.campaign',
  'facepass.registro',
  'facepass.rfids',
  'facepass.adminlogin'
])

.factory('httpRequestInterceptor', ['$cookieStore', function ($cookieStore) {
  return {
    request: function (config) {
      var token = $cookieStore.get("admin_token");
      //config.headers['Authorization'] = 'Token token="' + token + '"';
      config.headers['Authorization'] = token;
      return config;
    }
  };
}])

.config(['$routeProvider','$httpProvider', function($routeProvider, $httpProvider) {
	
	$routeProvider.
      otherwise({
        redirectTo: '/adminlogin'
    });

    $httpProvider.interceptors.push('httpRequestInterceptor');

}])
.controller('HeaderController', function($scope, AdminSession, $location, $route) {

  $scope.logout = function() {
    AdminSession.destroy();
    $location.path('/adminlogin');
  };

})
.run( function($rootScope, $location, $cookieStore,$http) {
  // register listener to watch route changes
  $rootScope.$on( "$locationChangeStart", function(event, next, current) {
      if ( next.templateUrl != "adminlogin.html" ) {
        $http.post('/checklogin', {"token": $cookieStore.get("admin_token")})
        .success(function(data, status, headers, config) {
            console.log(data);
        })
        .error(function(data, status, headers, config) {
            $location.path( "/adminlogin" );
        });
      }
  });
});

