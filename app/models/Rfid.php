<?php

class Rfid extends \Eloquent {
	protected $fillable = [];
	public $incrementing = false;

	public function user()
    {
        return $this->belongsTo('User');
    }

    public function images()
    {
        return $this->hasMany('Images');
    }
}