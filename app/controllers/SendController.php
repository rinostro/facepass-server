<?php

class SendController extends \BaseController {
	
	private $rules = array(
        'rfid' => 'required',
        'rut'  => '',
        'email' => 'email',
        'campaign'=> ''
    );


	/**
	 * Display a listing of the resource.
	 * GET /send
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$sends = Send::lists('id');

		return Response::json(
			$sends,
	        200
	    );
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /send/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /send
	 *
	 * @return Response
	 */
	public function store()
	{

		$validator = Validator::make(Input::all(), $this->rules);
		if($validator->fails()) {
			return Response::json(
		    	array('error' => true, 'message' => $validator->messages()),
		        400
	    	);
		}

		$send->rfid=Input::get('rfid');
		$send->email= Input::get('email');
		$send->rut=Input::get('rut');
		$send->campaign="asd";


		if($send->save()) {
				return Response::json(
		    		$send,
		    	     200
		    	);
		}else {
				return Response::json(
			   	 array('error' => true, 'message' => 'Error guardando RFID'),
			   	 400
			   	 );
		}
	}

	/**
	 * Display the specified resource.
	 * GET /send/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$send = Send::find($id);

		return Response::json(
	        $send,
	        200
	    );
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /send/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /send/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /send/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$send  = Send::find($id);
		$send ->delete();

		return Response::json(array(
	        'error' => false),
	        200
	    );
	}

}