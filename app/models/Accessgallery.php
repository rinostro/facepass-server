<?php

class Accessgallery extends \Eloquent {
	protected $fillable = [];
	public $incrementing = false;

    public function user()
    {
        return $this->belongsTo('User');

	}
    public function campaign()
    {
        return $this->belongsTo('Campaign');
    }  
}