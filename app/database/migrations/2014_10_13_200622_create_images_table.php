<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('images', function($table)
		{
		    $table->increments('id');
		    $table->string('campaign_id', 45);
		    $table->string('user_id')->nullable();
		    $table->string('totem_id', 45)->nullable();
		    $table->string('rfid_id', 20);
		    $table->string('image_name', 255);
		    $table->string('image_facebook_id', 255)->nullable();
		    $table->string('image_checksum', 32);
		    $table->integer('status_publication')->default(0);
		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('images');
	}

}
