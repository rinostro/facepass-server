<?php

class AccessgalleryController extends \BaseController {

	private $rules = array(
        'rut'  => '',
        'email' => 'email',
        'campaign'=> '',
        'status_access'=> ''

    );
	/**
	 * Display a listing of the resource.
	 * GET /accessgallery
	 *
	 * @return Response
	 */
	public function index()
	{
		$accessgallery = Send::where('status_access',1)->select('id','rut','email','campaign')->get();

		return Response::json(
			$accessgallery,
	        200
	    );
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /accessgallery/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /accessgallery
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), $this->rules);
		if($validator->fails()) {
			return Response::json(
		    	array('error' => true, 'message' => $validator->messages()),
		        400
	    	);
		}

		$accessgallery->email= Input::get('email');
		$accessgallery->rut=Input::get('rut');
		$accessgallery->campaign=Input::get('campaign');
		$accessgallery->status_access=1;


		if($accessgallery>save()) {
				return Response::json(
		    		$send,
		    	     200
		    	);
		}else {
				return Response::json(
			   	 array('error' => true, 'message' => 'Error guardando Access a gallery'),
			   	 400
			   	 );
		}
	}

	/**
	 * Display the specified resource.
	 * GET /accessgallery/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /accessgallery/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /accessgallery/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /accessgallery/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}