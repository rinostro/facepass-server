<?php

session_start();
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\GraphUser;

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//		
		$users = User::lists('id');

		return Response::json(array(
	        'error' => false,
	        'users' => $users),
	        200
	    );

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$access_token = Input::get('access_token');
		try {
			$session = new FacebookSession($access_token);
			$session = $session->getLongLivedSession();
			$graphUser = (new FacebookRequest(
			  $session, 'GET', '/me'
			))->execute()->getGraphObject(GraphUser::className());

		}catch(FacebookSDKException $e) {
			return Response::json(array(
		        'error' => true,
		        'message' => $e),
		        400
		    );
		}
		
		$rfid=Input::get('rfid');
		$id = $graphUser->getId();
		$user = User::firstOrNew(array('id' => $id));
		$isNew = false;
		if(empty($user->id)) {
			$isNew = true;
		}
	    $user->rut = Input::get('rut');//Input::get('rut');
	    $user->password = Input::get('password');
	    $user->first_name = $graphUser->getProperty('first_name');
	    $user->last_name = $graphUser->getProperty('last_name');
	    $user->access_token = $session->getToken();
	    $user->email = $graphUser->getProperty('email');
	    $user->friends = Input::get('friends');
	    $user->age_range = $graphUser->getProperty('age_range');
	    $user->gender = $graphUser->getProperty('gender');
	    $user->url_img = 'https://graph.facebook.com/'.$graphUser->getId().'/picture?width=175&height=175';
	    $user->link = $graphUser->getLink();
	    //$user->type = Input::get('type');
	    //$user->active = Input::get('active');
	    $info = $session->getSessionInfo();
	    $user->token_date = $info->getExpiresAt();



	    // Validation and Filtering is sorely needed!!s	    
	 	
	    $user->save();
	 
	 	if(!empty($rfid)) {
	 		$rfid_existe = Rfid::find($rfid);
	 		if(!empty($rfid_existe)) {
	 			$rfid_existe->user_id = $user->id;
	 			$rfid_existe->save();
	 		} else {	
	 			$rfid_new = new Rfid;
		 		$rfid_new->id = $rfid;
		 		$rfid_new->type = 'propia';
		 		$rfid_new->user_id = $user->id;
		 		$rfid_new->save();
	 		}	
	 	}


	    return Response::json(array(
	        'error' => false,
	        'user' => $user->toArray(),
	        'nuevo' => $isNew),
	        200
	    );
	    
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$user = User::find($id);

		return Response::json($user->toArray(),
	        200
	    );
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$user = User::find($id);
	    $user->type = Request::get('type');
	    $user->facebook_id = Request::get('facebook_id');
	    $user->password = Request::get('password');
	    $user->first_name = Request::get('first_name');
	    $user->last_name = Request::get('last_name');
	    $user->access_token = Request::get('access_token');
	    $user->email = Request::get('email');
	    $user->friends = Request::get('friends');
	    $user->age_range = Request::get('age_range');
	    $user->gender = Request::get('gender');
	    $user->active = Request::get('active');
	    $user->token_date = Request::get('token_date');
	 
	    // Validation and Filtering is sorely needed!!
	    // Seriously, I'm a bad person for leaving that out.
	 
	    $user->save();
	 
	    return Response::json(array(
	        'error' => false,
	        'user' => $user->toArray()),
	        200
	    );
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$user = User::find($id);
		$user->delete();

		return Response::json(array(
	        'error' => false),
	        200
	    );
	}


}
