<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('users', function($table)
		{
		    
		    $table->string('id', 100)->primary(); //facebook_id
		    $table->string('rut', 10);
		    $table->string('type', 10)->default('user');
		    $table->string('username', 45)->nullable();
		    $table->string('password', 45)->nullable();
		    $table->string('first_name', 45)->nullable();
		    $table->string('last_name', 45)->nullable();
		    $table->string('access_token', 250)->nullable();
		    $table->string('email', 150)->nullable();
		    $table->string('url_img', 150)->nullable();
		    $table->string('link', 150)->nullable();
		    $table->integer('friends')->nullable();
		    $table->string('age_range', 45)->nullable();
		    $table->string('gender', 20)->nullable();
		    $table->boolean('active')->default(0);
		    $table->timestamp('token_date')->nullable();
		    $table->timestamps();
		    $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('users');
	}

}
