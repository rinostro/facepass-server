'use strict';

angular.module('facepass', [
	'ngRoute',
	'ngResource',
	'ngCookies',
	'facepass.services',
	'facepass.directives',
	'facepass.preregistro',
	'facepass.perfil',
	'facepass.registromail',
  'facepass.postregistro'
])

.factory('httpRequestInterceptor', ['$cookieStore', function ($cookieStore) {
  return {
    request: function (config) {
      var token = $cookieStore.get("access_token");
      //config.headers['Authorization'] = 'Token token="' + token + '"';
      config.headers['Authorization'] = token;
      return config;
    }
  };
}])

.config(['$routeProvider','$httpProvider', 'ezfbProvider', function($routeProvider, $httpProvider, ezfbProvider) {
	$routeProvider.
      otherwise({
        redirectTo: '/preregistro'
  });

  $httpProvider.interceptors.push('httpRequestInterceptor');

  ezfbProvider.setLocale('es_LA');

  ezfbProvider.setInitParams({
      appId: '1522630301321893',
      version: 'v2.0'
  });
}])

.controller('HeaderController', function($scope, Session, $location, $cookieStore, $route) {

  $scope.status = $cookieStore.get("access_token");

  $scope.logout = function() {
    Session.destroy();
    $location.path('/preregistro');
  };

})
.run( function($rootScope, $location, $cookieStore,$http) {
  // register listener to watch route changes
  $rootScope.$on( "$locationChangeStart", function(event, next, current) {
     var sinparameters= next.split("?");
      if ( sinparameters[0] != "http://localhost/#/preregistro" 
        && sinparameters[0] !="http://localhost/#/postregistro"
        && sinparameters[0] != "http://localhost/#/registromail"

        && sinparameters[0] != "http://www.localhost/#/preregistro" 
        && sinparameters[0] !="http://www.localhost/#/postregistro"
        && sinparameters[0] != "http://www.localhost/#/registromail") {
        $http.post('/checkloginuser', {"token": $cookieStore.get("access_token")})
        .success(function(data, status, headers, config) {
            console.log(data);
        })
        .error(function(data, status, headers, config) {
            $location.path( "/preregistro" );
        });
      }
      else if(sinparameters[0]=="http://localhost/#/postregistro" && sinparameters[0]=="http://www.localhost/#/postregistro" ){
          
          if(sinparameters.lengt ==2)
              $location.path( "/postregistro?"+sinparameters[1]);
          else
             $location.path( "/postregistro");
      }
      else if(sinparameters[0]=="http://localhost/#/registromail" && sinparameters[0]=="http://www.localhost/#/registromail"){
           if(sinparameters.lengt ==2)
              $location.path( "/registromail?"+sinparameters[1]);
          else
             $location.path( "/registromail");

      }
  });
});
