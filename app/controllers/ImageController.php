<?php

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\GraphUser;

class ImageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$campaign_id = Input::get('campaign_id');
			

		// filtrar por imagenes que no poseen user_id y permanezcan a la campaña
		$images = Image::whereNull('user_id' )
						->where('campaign_id',$campaign_id)
						->groupBy('image_checksum')
						->orderBy('id')
						->lists('id');
						
		$images_name = Image::whereNull('user_id' )
						->where('campaign_id',$campaign_id)
						->groupBy('image_checksum')
						->orderBy('id')
						->lists('image_name');
		/*
		$checksum= Image::whereNull('user_id' )
						->where('campaign_id',$campaign_id)
						->select('image_checksum')
						->groupBy('image_checksum')
						->orderBy('id')
						->get();

		
		$rfids_image= array();

		foreach ($checksum as $image) {
	
		$rfids_image[]=Image::where('image_checksum', $image->image_checksum)
				->lists('rfid_id');

		}*/
			return Response::json(array(
	         'error' => false,
	         'images' => $images,
	         'images_name'=>$images_name),
			 //'rfids_image' =>$rfids_image),
	         200
	     );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$image = Image::find($id);
		$ruta = null;

		if(!empty($image)) {
			$ruta = $image->campaign_id . '_' . $image->image_checksum;
		}

		if(!empty($id) && File::exists(__DIR__.'/../storage/images/' . $ruta . '.jpg')) {
			$img = Intervention::make(app_path() . '/storage/images/' . $ruta . '.jpg');
			return $img->response('jpg');
		} else {
			return 'error';
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$image_checksum = Input::get('image_checksum');
		$rfids = Input::get('rfids');
		$campaign_id = Input::get('campaign_id');
		$stored = false;

		if (!empty($image_checksum)
			&& !empty($rfids)
			&& !empty($campaign_id)
			&& Input::hasFile('image') 
			&& Input::file('image')->isValid())
		{
			$extension = Input::file('image')->getClientOriginalExtension();
			$real_path = Input::file('image')->getRealPath();
			$image_name = $campaign_id . '_' . $image_checksum . '.' . $extension;

			if($image_checksum == md5_file($real_path)) {
	
				// obteniendo datos de campaña
				$campaign = Campaign::where('name',$campaign_id)->first();

				//Si no existe campaña la genero por su name
				if(empty($campaign)){
					$newCampaign= new Campaign;
					$newCampaign->id = str_random(20);
					$newCampaign->name= $campaign_id;
					$newCampaign->save();
				}
				
				// por cada rfid intentar subir a face.
				foreach ($rfids as $rfid_id) {
					// guardando imagen
					if($stored == false) {
						//Input::file('image')->move(__DIR__ . '/../storage/images', $image_name);
						Input::file('image')->move(__DIR__ . '/../../public/images', $image_name);
						$stored = true;
					}

					$rfid_user_id = null;
					$image_facebook_id = null;

					// obtiene id del usuario a partir de rfid
					$rfid = Rfid::where('id', $rfid_id)->first();
					if(!empty($rfid)) {
						$rfid_user_id = $rfid->user_id;
					}
					//Si no existe ese RFID, se crea como propia
					else{
						$newRfid= new Rfid;
						$newRfid->id= $rfid_id;
						$newRfid->type="propia";
						$newRfid->save();
					}

					// sube la imagen a facebook, si resulta, se obtiene el id de la imagen en facebook
					$status_publication=0;
					if(!empty($rfid_user_id)) {
						$image_facebook_id = $this->facebookImageUpload(
							$rfid_user_id, 
							//__DIR__ . '/../storage/images/' . $image_name, 
							__DIR__ . '/../../public/images/' . $image_name, 
							$campaign->facebook_text
						);
						$status_publication=1;
					}

					// guardando datos en la bd
					$image = null;
					$image = Image::firstOrNew(array('campaign_id' => $campaign_id, 'rfid_id' => $rfid_id, 'image_checksum' => $image_checksum));
					$image->user_id = $rfid_user_id;
					$image->rfid_id = $rfid_id;
					$image->campaign_id = $campaign_id;
					$image->image_name = $image_name;
					$image->image_checksum = $image_checksum;
					$image->image_facebook_id = $image_facebook_id;
					$image->status_publication= $status_publication;

					if(!$image->save()) {
					    return Response::json(array(
					        'error' => true,
					        'message' => 'Error on save, rfid: ' . $rfid_id,
					        'image' => $image,
					        400
					    ));
					}
				}

			} else {
			    return Response::json(array(
			        'error' => true,
			        'message' => 'Checksum Error',
			        //'image' => $image,
			        400
			    ));
			}
		} else {
		    return Response::json(array(
		        'error' => true,
		        'message' => 'Input Error check: image, image_checksum, rfids, campaign_id',
		        'image' => $image,
		        400
		    ));
		}

	    return Response::json(array(
	        'error' => false,
	        'message' => 'OK',
	        'image' => $image,
	        200
	    ));
	}


	private function facebookImageUpload($user_id, $image_path, $message) {
		$user = User::find($user_id);

		if(empty($user->access_token))
			return;

		$session = new FacebookSession($user->access_token);

		if($session) {

		  try {

		    // Upload to a user's profile. The photo will be in the
		    // first album in the profile. You can also upload to
		    // a specific album by using /ALBUM_ID as the path     
		    $response = (new FacebookRequest(
		      $session, 'POST', '/me/photos', array(
		        'source' => new CURLFile($image_path, 'image/jpeg'),
		        'message' => $message
		      )
		    ))->execute()->getGraphObject();

		    // If you're not using PHP 5.5 or later, change the file reference to:
		    // 'source' => '@/path/to/file.name'

		    return $response->getProperty('id');

		  } catch(FacebookRequestException $e) {

		  	return;
		    //echo "Exception occured, code: " . $e->getCode();
		    //echo " with message: " . $e->getMessage();

		  }   

		}
	}


	

	public function updateUserId() {

		$user_id = Input::get('user_id');
		$rfid = Input::get('rfid');
		$campaign_id= Input:: get('campaign_id');

		$user = User::find($user_id);


		if(!empty($user) && empty($campaign_id)) {
			Image::whereNull('user_id')->where('rfid_id', $rfid)->update(array('user_id' => $user_id));	


			return Response::json(array(
	        'error' => false,
	        'message' => 'OK',
	        200
	    ));
		}

		else if(!empty($user) && !empty($campaign_id)) {
			Image::whereNull('user_id')->where('rfid_id', $rfid)
									   ->where('campaign_id', $campaign_id)
									   ->update(array('user_id' => $user_id));	


		}

		else{ 
			return Response::json(array(
	        'error' => true,
	        400
	    	));
		}
		//Falta agregar los result!!

		//lo agregué como llamada post a image/updateuserid, 
		//asi que para ejecutarlo tienes que hacer una llamada post en angular, y darle como parametros
		//user_id y rfid
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /image/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)	{
	

		return Response::json(array(
	         'error' => false),
	         200
	     );	
	}

	public function galleryUser(){

		$user_id= Input:: get('user_id');
		$user = User::find($user_id);
		$status_publication =Input:: get('status_publication');
		$campaign_id= Input:: get('campaign_id');

		if(!empty($user)){

			if(!empty($campaign_id)){	

			$images = Image::where('user_id', $user_id)
							->where('campaign_id',$campaign_id)	
							->where('status_publication', $status_publication)					
							->groupBy('image_checksum')
							->orderBy('id')
							//->paginate(20)
							->lists('id');
			
			return Response::json(array(
		         'error' => false,
		         'images' => $images),
		         200
		     	);
		    }else{

		    	$images = Image::where('user_id', $user_id)
							->where('status_publication', $status_publication)					
							->groupBy('image_checksum')
							->orderBy('id')
							//->paginate(20)
							->lists('id');

				return Response::json(array(
		         'error' => false,
		         'images' => $images),
		         200
		     	);

		    }	
		}

		else{
			return Response::json(array(
	        'error' => true,
	        400
	    	));
		}


		
	}

	public function publicarPendientes(){

		$user_id= Input::get('user_id');
	
		$images = Image::where('user_id', $user_id)
							->where('status_publication', 0)					
							->groupBy('image_checksum')
							->orderBy('id')
							->lists('id');

		//Ciclo for $images es una lista de ids de imagenes
		foreach ($images as $imageid) {
					
			$image = null;
			$image = Image::firstOrNew(array('id' => $imageid));
			$image_name= $image->image_name;
			$campaign = Campaign::where('name',$image->campaign_id)->first();


			$status_publication=0;
			$image_facebook_id=null;

			if(!empty($user_id)) {
				$image_facebook_id = $this->facebookImageUpload(
					$user_id, 
					//__DIR__ . '/../storage/images/' . $image_name, 
					__DIR__ . '/../../public/images/' . $image_name, 
					$campaign->facebook_text
					);
				$status_publication=1;
			}

			// guardando datos en la bd

			$image->image_facebook_id = $image_facebook_id;
			$image->status_publication= $status_publication;
			// File::delete(__DIR__.'/../storage/images/'.$image->image_name);

			if(!$image->save()) {
				return Response::json(array(
					'error' => true,
					'message' => 'Error on save, imageid: ' . $imageid,
					'image' => $imageid,
					400
					));
			}
		}


		return Response::json(array(
			'error' => false,
		    'images' => $images),
		    200
		    );
	}

	public function publicarFacebook() {
		

		$user = User::find(Input::get('user_id'));
		$image= Image::where('id',Input::get('image_id'))->first();
		//$image_path= __DIR__ . '/../storage/images/' . $image->image_name;
		$image_path= __DIR__ . '/../../public/images/' . $image->image_name;
		$campaign = Campaign::where('name',$image->campaign_id)->first();

		if(empty($user->access_token))
			return;

		$session = new FacebookSession($user->access_token);

		if($session) {

		  try {

		    // Upload to a user's profile. The photo will be in the
		    // first album in the profile. You can also upload to
		    // a specific album by using /ALBUM_ID as the path     
		    $response = (new FacebookRequest(
		      $session, 'POST', '/me/photos', array(
		        'source' => new CURLFile($image_path, 'image/jpeg'),
		        'message' => $campaign->facebook_text
		      )
		    ))->execute()->getGraphObject();

		    // If you're not using PHP 5.5 or later, change the file reference to:
		    // 'source' => '@/path/to/file.name'
		    $image->status_publication=1;
		    $image->user_id= $user->id;
		    $image->image_facebook_id =  $response->getProperty('id');
		    $image->save();
		    //File::delete(__DIR__.'/../storage/images/'.$image->image_name);

		    return $response->getProperty('id');

		  } catch(FacebookRequestException $e) {

		  	return;
		    //echo "Exception occured, code: " . $e->getCode();
		    //echo " with message: " . $e->getMessage();

		  }   

		}
	}

	public function obtenerModa(){

		$ids_images= Input::get('ids_images');
		
		$rfids_image= array();

		foreach ($ids_images as $image) {
		
		$rfids_image[]=Image::where('id',$image)->lists('rfid_id');

		}
		
		return Response::json(array(
	         'error' => false,
	         'ids_images'=>$ids_images	,
	         'rfids_image' => $rfids_image),
	         200
	    );

	}



}








