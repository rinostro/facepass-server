'use strict';

angular.module('facepass.registro', [])

.config(['$routeProvider', function($routeProvider) {

	$routeProvider.when('/registro', {
		templateUrl: 'app/registro/registro.html',
		controller: 'RegistroController'
	});
}])

.controller('RegistroController', function($scope, Rfid) {
	$scope.alerts = [];

	$scope.guardar = function(rfid_in) {
		var rfid = new Rfid(rfid_in);
		$scope.alerts = [];
		rfid.$save(function(data){
			$scope.alerts.push({msg: data.message});
			$scope.rfid = {};
		},
		function(data){
			$scope.alerts.push({msg: data.data.message});
		});
	}

	$scope.closeAlert = function(index) {
    	$scope.alerts.splice(index, 1);
  	};
	
});