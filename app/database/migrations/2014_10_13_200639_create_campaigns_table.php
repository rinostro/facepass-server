<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('campaigns', function($table)
		{
			$table->string('id', 45)->primary();
			$table->string('name', 255)->nullable();
		    $table->integer('interval_time')->nullable();
		    $table->integer('sync_time')->nullable();
		    $table->string('frame_image_hd', 255)->nullable();
		    $table->string('frame_image_vga',255)->nullable();
		    $table->string('start_image', 255)->nullable();
		    $table->text('start_text')->nullable();
		    $table->text('end_text')->nullable();
		    $table->text('facebook_text')->nullable();
		    $table->timestamp('start_date')->nullable();
		    $table->timestamp('end_date')->nullable();
		    $table->softDeletes();
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('campaigns');
	}

}
