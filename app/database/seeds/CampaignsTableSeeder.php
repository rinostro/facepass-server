<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CampaignsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Campaign::create([
				'id' => $faker->userName,
				'name' => $faker->name,
				'interval_time' => $faker->randomDigitNotNull,
				'sync_time' => $faker->randomDigitNotNull,
				'frame_image_vga' => null,
				'frame_image_hd' => null,
				'start_image' => null,
				'start_text' => $faker->text,
				'end_text' => $faker->text,
				'facebook_text' => $faker->text,
				'start_date' => $faker->dateTime(),
				'end_date' => $faker->dateTime()
			]);
		}
	}

}