<?php

class CampaignController extends \BaseController {

	private $rules = array(
        'name' => 'required',
        'interval_time' => '',
        'sync_time' => '',
        'start_text' => '',
        'end_text' => '',
        'facebook_text' => '',
        'start_date' => '',
        'end_date' => '',
        'frame_image_vga' => '',
        'frame_image_hd' => '',
        'start_image' => ''

    );

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$campaigns = Campaign::select('name','id')->orderBy('name')->get();

		return Response::json($campaigns,
	        200
	    );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$validator = Validator::make(Input::all(), $this->rules);
		if($validator->fails()) {
			return Response::json(
		    	array('error' => true, 'message' => $validator->messages()),
		        400
	    	);
		}


	 if(empty( Campaign::where('name',Input::get('name'))->first())) {

		$campaign = new Campaign;
	    $campaign->id = str_random(20);
	    $campaign->name = Input::get('name');
	    $campaign->start_date = Input::get('start_date');
	    $campaign->end_date = Input::get('end_date');
	  
	  /*  $campaign->interval_time = Input::get('interval_time');
	    $campaign->sync_time = Input::get('sync_time');
	    $campaign->start_text = Input::get('start_text');
	    $campaign->end_text = Input::get('end_text');
	    $campaign->facebook_text = Input::get('facebook_text');
	 

		if (Input::hasFile('frame_image_vga') ){
	    $campaign->frame_image_vga = Input::get('frame_image_vga');
	    Input::file('frame_image_vga')->move(__DIR__ . '/../storage/campaign/'.$campaign->name, "frame_vga.png" );
		}

		if (Input::hasFile('frame_image_hd') ){
	    $campaign->frame_image_hd = Input::get('frame_image_hd');
	    Input::file('frame_image_hd')->move(__DIR__ . '/../storage/campaign/'.$campaign->name, "frame_hd.png" );
		}

		if (Input::hasFile('start_image') ){
	    $campaign->start_image = Input::get('start_image');
	    Input::file('start_image')->move(__DIR__ . '/../storage/campaign/'.$campaign->name, "background.png" );
		}
		*/
	    $campaign->save();
	 
	    return Response::json($campaign,
	        200
	    );
	}

	else{

		  return Response::json(
		  	array('error' => true,'message'=>'Ya existe una campania con ese nombre'),
		  	400
		);
	}

}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$campaign = Campaign::find($id);

		if (empty($campaign)) {
			return Response::json(array('error' => true),
	        	400
	    	);
		}

		return Response::json(array(
				'error' => false,
				'message' => 'Success in show campaign: ' . $campaign->name,
				'campaign' => $campaign,
				200
				));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
		$campaign = Campaign::find($id);

		if(!empty(Input::get('name'))){

	    	$id_campaign=Campaign::where('name', Input::get('name'))->select('id')->first();

	    	if(!empty($id_campaign)){
	    		if($id_campaign->id != $id){
	    			return Response::json(array('error' => true,'message' => 'Error in edit campaign: '),
		        	400
		    		);
	      		}
	      	}

	    }

	    if(!empty(Input::get('start_date')))
	    	$campaign->start_date = Input::get('start_date');

	    if(!empty(Input::get('end_date')))
	   		$campaign->end_date = Input::get('end_date');
	 	

	 	$campaign->name = Input::get('name');
	    $campaign->save();
	 
	    return Response::json(array(
				'error' => false,
				'message' => 'Success in update campaign: ' . $campaign->name,
				'campaign' => $campaign,
				200
				));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$campaign = Campaign::find($id);
		$campaign->delete();

		return Response::json(array(
	        'error' => false),
	        200
	    );
	}


}
