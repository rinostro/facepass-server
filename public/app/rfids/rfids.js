'use strict';

angular.module('facepass.rfids', ['ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {

	$routeProvider.when('/rfids', {
		templateUrl: 'app/rfids/rfids.html',
		controller: 'RfidsController'
	});
}])

.controller('RfidsController', function($scope, $modal, Rfid, $log) {
	var rfids = Rfid.query(function() {
		$scope.rfids = rfids;
	});

	$scope.open = function (size) {

		var modalInstance = $modal.open({
			templateUrl: 'myModalContent.html',
			controller: 'ModalSaveRfidCtrl',
			size: size,
			resolve: {
			}
		});

		modalInstance.result.then(function (item) {
			//console.log(item);
			//$scope.rfids.push(item);
			var rfids = Rfid.query(function() {
				$scope.rfids = rfids;
			});
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	$scope.openFileModal = function (size) {

		var modalInstance = $modal.open({
			templateUrl: 'modalFileUploadContent.html',
			controller: 'ModalFileRfidCtrl',
			size: size,
			resolve: {
			}
		});

		modalInstance.result.then(function (item) {
			//console.log(item);
			//$scope.rfids.push(item);
			var rfids = Rfid.query(function() {
				$scope.rfids = rfids;
			});
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};

	$scope.remove = function(index) {
		var rfid = $scope.rfids[index];
		Rfid.remove(rfid, function() {
			$scope.rfids.splice(index, 1);
		});
	};


})

.controller('ModalSaveRfidCtrl', function ($scope, $modalInstance, $http, Rfid) {
	$scope.alerts = [];

	$scope.guardar = function(rfid_in) {
		var rfid = new Rfid();
		rfid.id = rfid_in.id;
		rfid.type = 'arriendo';
		$http.post('/rfid/storeArriendo', rfid).
		success(function(data, status, headers, config) {
			$modalInstance.close(rfid);
		}).
		error(function(data, status, headers, config) {
			$scope.alerts.push({msg: data.message});
		})
	};

	$scope.cancel = function() {
		$scope.rfid = {};
		$modalInstance.dismiss('cancel');
	};

	$scope.ok = function () {
    //$modalInstance.close();
	};

	$scope.closeAlert = function(index) {
    	$scope.alerts.splice(index, 1);
  	};

})
.controller('ModalFileRfidCtrl', function ($scope, $modalInstance, Rfid, fileUpload) {

	$scope.guardar = function() {
		console.log($scope.myFile);
		
		var file = $scope.myFile;
		var uploadUrl = '/rfid/fileUpload';
		fileUpload.uploadFileToUrl(file, uploadUrl);
		$modalInstance.close();
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};

	$scope.ok = function () {
    //$modalInstance.close();
	};

});