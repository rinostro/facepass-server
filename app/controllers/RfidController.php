<?php

class RfidController extends \BaseController {

	private $rules = array(
        'id' => 'required|size:10',
        'rut' => 'min:8',
        'email' => 'email'
    );

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$rfids = Rfid::where('type', 'arriendo')->get();

		return Response::json(
			$rfids,
	        200
	    );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//Faltan validaciones y el caso en que Input::get('email') venga seteado
		//parametros: rut, id y email
		$validator = Validator::make(Input::all(), $this->rules);
		if($validator->fails()) {
			return Response::json(
		    	array('error' => true, 'message' => $validator->messages()),
		        400
	    	);
		}
		
		if(!empty(Input::get('rut')))
			{
			$user = User::where('rut', Input::get('rut'))->first();
			}
		
		$rfid = Rfid::find(Input::get('id'));

		//En caso de post-registro se busca por el id directamente
		if(!empty(Input::get('userid')))
		$userFindforid = User::where('id', Input::get('userid'))->first();


		//Caso de registro-mail o registro sin pre-registro: Input de mail no es vacio
		if(!empty(Input::get('email')) && empty($user)) {

			//enviar email con direccion de registro y rfid en parametro
			//mandar un email a ese mail con una url para que se pueda registrar 
			$url_registro = URL::to('/').'#/registromail?rfid='.Input::get('id');

			//Registro envio de mail
			$send= new Send;
		    $send->rfid=Input::get('id');
			$send->email= Input::get('email');
			
			if(!empty(Input::get('rut')))
			{
			$send->rut= Input::get('rut');
			}
			else{
			$send->rut="";
			}

			$send->campaign="";

			$send->save();

			Mail::send('emails.registro', array('url' => $url_registro), function($message)
			{
 				  $message->to(Input::get('email'), 'Usted')->subject('Welcome!');
			});
			
			return Response::json(
		    	array('error' => false, 'message' => 'Pre-registro. Se envia RFID'),
		        200
		    );
		}

		//Caso de registro
		else if(!empty($user)) {
			if(!empty($rfid)) {
				$rfid->user_id = $user->id;
				$rfid->save();
				//Deberia actualizar imagenes con ese rfid pero por campaña...
				 Image::whereNull('user_id')->where('rfid_id', $rfid->id)->update(array('user_id' => $user->id));	 
			} else {
				$rfid = new Rfid;
				$rfid->id = Input::get('id');
				$rfid->user_id = $user->id;
				$rfid->type = 'propia';	
				//Deberia actualizar imagenes con ese rfid pero por campaña...
				 Image::whereNull('user_id')->where('rfid_id', $rfid->id)->update(array('user_id' => $user->id));	 
					
			}

			//Si token esta vencido
			if(DateTime::createFromFormat('Y-m-d H:i:s', $user->token_date) < new DateTime() ) {

				Mail::send('emails.tokenvencido', array('url' => URL::to('/')), function($message)
					{		
						  $user = User::where('rut', Input::get('rut'))->first();
		 				  $message->to($user->email, 'Usted')->subject('Token vencido de Facebook!');
					});
				dd('token vencido por fecha');

				//Registro envio de mail
			    $send= new Send;
		    	$send->rfid=Input::get('id');
				$send->email= $user->email;
				$send->rut=$user->rut;
				$send->campaign="asd";


				$send->save();					

			}

			if($rfid->save()) {
				return Response::json(
					array('error' => false, 'message' => 'Guardado!', 'rfid' => $rfid),
		        	200
		    	);
			} else {
				return Response::json(
			    	array('error' => true, 'message' => 'Error guardando RFID'),
			        400
			    );
			}
		}

		//Caso de post-registro
		else if(!empty($userFindforid)){

			if(!empty($rfid)) {
				$rfid->user_id = $userFindforid->id;
				$rfid->save();
				//Deberia actualizar imagenes con ese rfid pero por campaña...
				 Image::whereNull('user_id')->where('rfid_id', $rfid->id)->update(array('user_id' => $userFindforid->id));	 
			} else {
				$rfid = new Rfid;
				$rfid->id = Input::get('id');
				$rfid->user_id = $userFindforid->id;
				$rfid->type = 'propia';	
				//Deberia actualizar imagenes con ese rfid pero por campaña...
				 Image::whereNull('user_id')->where('rfid_id', $rfid->id)->update(array('user_id' => $userFindforid->id));	 
					
			}

			//Si token esta vencido
			if(DateTime::createFromFormat('Y-m-d H:i:s', $userFindforid->token_date) < new DateTime() ) {

				Mail::send('emails.tokenvencido', array('url' => URL::to('/')), function($message)
					{		
						   $message->to($userFindforid->email, 'Usted')->subject('Token vencido de Facebook!');
					});
				dd('token vencido por fecha');

				//Registro envio de mail
			    $send= new Send;
		    	$send->rfid=Input::get('id');
				$send->email= $userFindforid->email;
				$send->rut=$userFindforid->rut;
				$send->campaign="asd";


				$send->save();					

			}

			if($rfid->save()) {
				return Response::json(
					array('error' => false, 'message' => 'Guardado!', 'rfid' => $rfid),
		        	200
		    	);
			} else {
				return Response::json(
			    	array('error' => true, 'message' => 'Error guardando RFID'),
			        400
			    );
			}


		}

		else{

		    return Response::json(
		    	array('error' => true, 'message' => 'No existe el usuario'),
		        400
		    );
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$rfid = Rfid::find($id);

		return Response::json(
	        $rfid,
	        200
	    );
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$rfid = Rfid::find($id);
		$rfid->user_id = Input::get('user_id');
		$rfid->type = Input::get('type');
	 
	    // Validation and Filtering is sorely needed!!
	    // Seriously, I'm a bad person for leaving that out.
	 
	    $rfid->save();
	 
	    return Response::json(
	    	$rfid,
	        200
	    );
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$rfid = Rfid::find($id);
		$rfid->delete();

		return Response::json(array(
	        'error' => false),
	        200
	    );
	}

	/**
	 * Lee archivo con listado de RFID y los carga a la base de datos
	 *
	 * @return Response
	 */
	public function rfidFileUpload()
	{
		if (Input::hasFile('file') && Input::file('file')->isValid())
		{
			
			$extension = Input::file('file')->getClientOriginalExtension();
			$real_path = Input::file('file')->getRealPath();
			
			$results=Excel::load( $real_path, function($reader) {

				return $reader->get()->groupBy('firstname');;
    		});		

			
			foreach( $results->parsed as $row ){
					$rfid= new Rfid();
					$rfid->id= $row->id;
					$rfid->type= "arriendo";
					$rfid->save();
			}

			return Response::json(array(
			    	'error'=>false,
			    	'message'=>"Subido archivo excel ",
			    	'extension'=>$extension,
			    	'real_path'=>$real_path
			    	//,'contenido'=>$results->parsed
			    	),
			        200
			    );
		}
	}

	/**
	 * Guarda un rfid de arriendo
	 *
	 * @return Response
	 */
	public function storeArriendo()
	{
		$validator = Validator::make(Input::all(), array('id' => 'required|min:10|unique:rfids'));
		if($validator->fails()) {
			return Response::json(
		    	array('error' => true, 'message' => $validator->messages()),
		        400
	    	);
		}

		$rfid = new Rfid;
		$rfid->id = Input::get('id');
		$rfid->type = 'arriendo';

		if($rfid->save()) {
			return Response::json(
	    		$rfid,
	        	200
	    	);
		} else {
			return Response::json(
		    	array('error' => true, 'message' => 'Error guardando RFID'),
		        400
		    );
		}
	}

}