'use strict';

angular.module('facepass.adminlogin', [])

.config(['$routeProvider', function($routeProvider) {

	$routeProvider.when('/adminlogin', {
		templateUrl: 'app/adminlogin/adminlogin.html',
		controller: 'AdminLoginController'
	});

}])

.controller('AdminLoginController', function($scope, $location, $routeParams,$http,AdminSession) {

	$scope.alerts = [];
  $scope.login=function(admin){

    $http.post('/adminlogin', admin).
    success(function(data, status, headers, config) {
      console.log(data.token + " "+ data.username);
      AdminSession.create(data.token, data.username);
      $location.path('/registro');
    }).
    error(function(data, status, headers, config) {
         console.log("Error");
         alertify.alert("Nombre de usuario y/o contrasena incorrecta");
    })
  }
	
})

.directive('validacionrut', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.integer = function(modelValue, viewValue) {
        if (ctrl.$isEmpty(modelValue)) {
          // consider empty models to be valid
          return true;
        }

        if (true) {
        	//aqui validacion rut
          	// it is valid
          return true;
        }

        // it is invalid
        return false;
      };
    }
  };
});